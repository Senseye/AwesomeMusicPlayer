package main

import (
	"amp"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	http.HandleFunc("/api/v1/user/auth/prototype", amp.UserAuthPrototypeHandler)
	http.HandleFunc("/api/v1/tracklist/main/create", amp.TracklistMainCreate)
	http.HandleFunc("/api/v1/statistic/song/listening/log", amp.StatisticSongListeningLog)
	http.HandleFunc("/api/v1/statistic/song/listening", amp.StatisticSongListening)

	http.HandleFunc("/develop/only/apidoc", amp.ApiDoc)
	http.HandleFunc("/develop/only/database/clear", amp.CommandHandler(amp.DatabaseMigrate))
	http.HandleFunc("/develop/only/statistic/song/listening/update", amp.CommandHandler(amp.ListeningStatisticUpdateCommand))

	http.HandleFunc("/", amp.DefaultHandler)
	http.ListenAndServe(":"+port, nil)
}
