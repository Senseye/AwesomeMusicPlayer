#!/usr/bin/env bash

# WARNING: only for development on local machine
# docker rm -f $(docker ps -a -q)
# docker ps -a

echo "Start common script"

function exec {
    INFO_COLOR='\033[0;32m'
    REMOVE_COLOR='\033[0m'

    printf "Try action: ${INFO_COLOR}${1}${REMOVE_COLOR}\n";

    if [ "$1" == "up" ]; then
        echo "Action $1 ..."

        sudo docker-compose down && sudo docker-compose up -d && sudo docker exec -it awesomemusicplayer_golang_1 sh

        echo "Action complete!"
    elif [ "$1" == "build" ]; then
        echo "Action $1 ..."

        sudo docker-compose down && sudo docker-compose up -d --build && sudo docker exec -it awesomemusicplayer_golang_1 sh

        echo "Action complete!"
    elif [ "$1" == "rebuild" ]; then
        echo "Action $1 ..."

        sudo docker stop awesomemusicplayer_golang_1 && sudo docker-compose up -d --build && sudo docker exec -it awesomemusicplayer_golang_1 sh

        echo "Action complete!"
    elif [ "$1" == "exec" ]; then
        echo "Action $1 ..."

        if [ "$2" == "app" ]; then
            echo "How run test?"
            echo "      go test"

            sudo docker exec -it awesomemusicplayer_golang_1 sh
        elif [ "$2" == "postgres" ]; then
            echo "How terminal|console postgres?"
            echo "      psql -U postgres"

            sudo docker exec -it awesomemusicplayer_postgres_1 sh
        else
            echo "Undefined container: $2"
            echo "Try: exec|postgres"
        fi

        echo "Action complete!"
    elif [ "$1" == "down" ]; then
        echo "Action $1 ..."

        sudo docker-compose down

        echo "Action complete!"
    else
        echo "Undefined command: $1"
        echo "Try: up|build|down|exec"
    fi
}

if [[ "$1" =~ ^(app|postgres)$ ]]; then
    exec exec $1
else
    exec $1 $2
fi
