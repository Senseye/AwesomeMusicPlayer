package amp

func CheckError(err error) {
	if err != nil {
		DebugError(err)
		panic(err)
	}
}
