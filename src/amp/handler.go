package amp

import (
	"fmt"
	"net/http"
	"time"
)

const (
	TOKEN_LENGTH = 32
)

func DefaultHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Awesome Music Player Service, path %s!", r.URL.Path[1:])
}

func UserAuthPrototypeHandler(w http.ResponseWriter, r *http.Request) {
	var request *AuthPrototypeRequest
	JsonDecode(r.Body, &request)

	token, err := getUserTokenByEmail(request.Email)

	if err != nil {
		HttpJsonError(w, err.Error(), http.StatusBadRequest)

		return
	}

	fmt.Fprintf(w, `{"token":"%s"}`, token)
}

func TracklistMainCreate(w http.ResponseWriter, r *http.Request) {
	var tracklist *TracklistMainRequest
	JsonDecode(r.Body, &tracklist)

	userId, err := GetUserIdByRequest(r)

	if err != nil {
		HttpJsonError(w, err.Error(), http.StatusUnauthorized)

		return
	}

	err = repositoryTracklistMainCreate(tracklist, userId)

	if err != nil {
		HttpJsonError(w, err.Error(), http.StatusBadRequest)

		return
	}

	fmt.Fprintf(w, JsonEncode(newTracklistMainResponse(tracklist)))
}

func StatisticSongListeningLog(w http.ResponseWriter, r *http.Request) {
	var statistic *StatisticSongListetingLogListRequest
	JsonDecode(r.Body, &statistic)

	repositoryStatisticSonglisteningLogStore(statistic)

	// TODO: only when development & testing
	ListeningStatisticUpdateCommand()
	// TODO: remove on production

	fmt.Fprintf(w, `{}`)
}

func StatisticSongListening(w http.ResponseWriter, r *http.Request) {
	userId, err := GetUserIdByRequest(r)

	if err != nil {
		HttpJsonError(w, err.Error(), http.StatusUnauthorized)

		return
	}

	fmt.Fprintf(w, JsonEncode(&StatisticSongListetingListResponse{
		Songs: getUserSongListeningStatistic(userId),
	}))
}

func CommandHandler(handler func() error) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		err := handler()
		duration := time.Now().Sub(startTime).Nanoseconds()

		message := ""
		if err != nil {
			message = err.Error()
		}

		fmt.Fprintf(w, `{"success":true,"duration":%d, "error":"%s"}`, duration, message)
	}
}

func HttpJsonError(w http.ResponseWriter, message string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	fmt.Fprint(w, message)
}
