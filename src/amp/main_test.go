package amp

import (
	"database/sql"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"testing"
	"time"
)

func init() {
	if err := DatabaseMigrate(); err != nil {
		fmt.Println("Initialize error on migrate:", err.Error())
	}
}

func TestWebServer(t *testing.T) {
	res := get("/awesome-message")
	defer res.Body.Close()

	assertSuccessStatus(t, res)
	assertResponse(t, "Awesome Music Player Service, path awesome-message!", res.Body)
}

func TestAuthPrototypeExistUser(t *testing.T) {
	json := `{"email":"senseye@awesome.player"}`

	res := post("/api/v1/user/auth/prototype", json)
	defer res.Body.Close()

	assertSuccessStatus(t, res)
	assertResponse(t, `{"token":"2519df20e8d1758304e579658c9fc669"}`, res.Body)
}

func TestAuthPrototypeNewUser(t *testing.T) {
	json := `{"email":"reen@awesome.player"}`

	res := post("/api/v1/user/auth/prototype", json)
	defer res.Body.Close()

	assertSuccessStatus(t, res)
	assertResponse(t, `{"token":"84062213b3f0f2043947052365704bc1"}`, res.Body)
}

func TestMainTracklistCreateEmptyAuthToken(t *testing.T) {
	json := getCreateUserSongRequest()

	res := post("/api/v1/tracklist/main/create?token=", json)
	defer res.Body.Close()

	assertResponseStatusCode(t, http.StatusUnauthorized, res.StatusCode)

	assertResponse(t, `{"error":"Invalid token format"}`, res.Body)
}

func TestMainTracklistCreateWrongToken(t *testing.T) {
	json := getCreateUserSongRequest()

	res := post("/api/v1/tracklist/main/create?token=11110000111100001111000011110000", json)
	defer res.Body.Close()

	assertResponseStatusCode(t, http.StatusUnauthorized, res.StatusCode)

	assertResponse(t, `{"error":"Wrong token"}`, res.Body)
}

func TestMainTracklistCreate(t *testing.T) {
	truncate([]string{
		"apm_tracklist",
		"apm_user_to_tracklist",
		"apm_tracklist_to_user_song",
		"amp_user_song",
		"amp_user_song_meta",
	})

	json := getCreateUserSongRequest()

	res := post("/api/v1/tracklist/main/create?token=2519df20e8d1758304e579658c9fc669", json)
	defer res.Body.Close()

	assertSuccessStatus(t, res)
	assertResponse(t, `{"songs":[{"user_song_id":1},{"user_song_id":2}]}`, res.Body)

	assertTableRows(
		t,
		"apm_tracklist",
		[]string{"tracklist_id", "named", "name"},
		[][]interface{}{
			{1, 0, nil},
		},
		func(rows *sql.Rows) []interface{} {
			var tracklistId, named int
			var name interface{}
			rows.Scan(&tracklistId, &named, &name)
			return []interface{}{tracklistId, named, name}
		},
	)

	assertTableRows(
		t,
		"apm_user_to_tracklist",
		[]string{"id", "user_id", "tracklist_id", "tracklist_position"},
		[][]interface{}{
			{1, 1, 1, 0},
		},
		func(rows *sql.Rows) []interface{} {
			var id, userId, tracklistId, tracklistPosition int
			rows.Scan(&id, &userId, &tracklistId, &tracklistPosition)
			return []interface{}{id, userId, tracklistId, tracklistPosition}
		},
	)

	assertTableRows(
		t,
		"apm_tracklist_to_user_song",
		[]string{"id", "tracklist_id", "user_song_id", "user_song_position"},
		[][]interface{}{
			{1, 1, 1, 1},
			{2, 1, 2, 2},
		},
		func(rows *sql.Rows) []interface{} {
			var id, tracklistId, userSongId, userSongPosition int
			rows.Scan(&id, &tracklistId, &userSongId, &userSongPosition)
			return []interface{}{id, tracklistId, userSongId, userSongPosition}
		},
	)

	assertTableRows(
		t,
		"amp_user_song",
		[]string{"user_song_id", "user_id", "song_id"},
		[][]interface{}{
			{1, 1, nil},
			{2, 1, nil},
		},
		func(rows *sql.Rows) []interface{} {
			var userSongId, userId int
			var songId interface{}
			rows.Scan(&userSongId, &userId, &songId)
			return []interface{}{userSongId, userId, songId}
		},
	)
}

func TestStatisticSongListeningLogCreate(t *testing.T) {
	truncate([]string{
		"apm_stat_user_song_listening_log",
	})

	json := `
{
    "songs": [
        {
            "user_song_id": 1,
			"complete_count": 5,
			"skip_count": 0
        },
        {
            "user_song_id": 2,
			"complete_count": 3,
			"skip_count": 0
        },
        {
            "user_song_id": 3,
			"complete_count": 1,
			"skip_count": 0
        }
    ]
}
`

	res := post("/api/v1/statistic/song/listening/log", json)
	defer res.Body.Close()

	assertSuccessStatus(t, res)
	assertResponse(t, `{}`, res.Body)
}

func TestEmptyStatisticSongListeningGet(t *testing.T) {
	truncate([]string{
		"apm_stat_user_song_listening",
	})

	res := get("/api/v1/statistic/song/listening?token=2519df20e8d1758304e579658c9fc669")

	assertSuccessStatus(t, res)
	assertResponse(t, `{"songs":[]}`, res.Body)
}

func TestStatisticSongListeningGet(t *testing.T) {
	truncate([]string{
		"amp_user_song",
		"apm_stat_user_song_listening",
	})
	now := time.Now()
	fillTable(
		"amp_user_song",
		[]string{"user_song_id", "user_id", "inserted"},
		[][]interface{}{
			{1, 1, now},
			{2, 1, now},
			{3, 1, now},
			{4, 1, now},
			{5, 1, now},
			{6, 1, now},
			{7, 1, now},
		},
	)

	fillTable(
		"apm_stat_user_song_listening",
		[]string{"user_song_id", "complete_count", "skip_count", "inserted", "updated"},
		[][]interface{}{
			{1, 5, 0, now, now},
			{2, 3, 0, now, now},
			{3, 2, 0, now, now},
			{4, 6, 0, now, now},
			{5, 8, 0, now, now},
			{7, 1, 0, now, now},
		},
	)

	res := get("/api/v1/statistic/song/listening?token=2519df20e8d1758304e579658c9fc669")
	assertSuccessStatus(t, res)

	assertResponse(
		t,
		`{"songs":[{"user_song_id":1,"complete_count":5},{"user_song_id":2,"complete_count":3},{"user_song_id":3,"complete_count":2},{"user_song_id":4,"complete_count":6},{"user_song_id":5,"complete_count":8},{"user_song_id":7,"complete_count":1}]}`,
		res.Body,
	)
}

func BenchmarkWebServer(b *testing.B) {
	json := getCreateUserSongRequest()

	for i := 0; i < b.N; i++ {
		res := post("/api/v1/tracklist/main/create?token=2519df20e8d1758304e579658c9fc669", json)
		res.Body.Close()
	}
}

func TestListeningStatisticUpdateCommand(t *testing.T) {
	truncate([]string{
		"apm_stat_user_song_listening",
		"apm_stat_user_song_listening_log",
	})

	now := time.Now()
	fillTable(
		"apm_stat_user_song_listening_log",
		[]string{"user_song_id", "complete_count", "skip_count", "inserted"},
		[][]interface{}{
			{1, 5, 0, now},
			{2, 3, 0, now},
			{3, 2, 0, now},
			{4, 6, 0, now},
			{5, 8, 0, now},
			{7, 1, 3, now},
			{7, 1, 5, now},
		},
	)

	ListeningStatisticUpdateCommand()

	assertTableRows(
		t,
		"apm_stat_user_song_listening",
		[]string{"user_song_id", "complete_count", "skip_count"},
		[][]interface{}{
			{1, 5, 0},
			{2, 3, 0},
			{3, 2, 0},
			{4, 6, 0},
			{5, 8, 0},
			{7, 2, 8},
		},
		func(rows *sql.Rows) []interface{} {
			var userSongId, completeCount, skipCount int
			rows.Scan(&userSongId, &completeCount, &skipCount)
			return []interface{}{userSongId, completeCount, skipCount}
		},
	)
}

func truncate(tables []string) {
	connect := SafeConnect()

	for _, table := range tables {
		stmt, err := connect.Prepare("TRUNCATE " + table + ";")
		CheckError(err)
		stmt.Exec()
	}
}

func fillTable(tableName string, columns []string, values [][]interface{}) {
	placeholders := make([]string, len(columns))

	for i := range columns {
		placeholders[i] = fmt.Sprintf("$%d", i+1)
	}

	query := fmt.Sprintf(
		"INSERT INTO %s (%s) VALUES(%s);",
		tableName,
		strings.Join(columns, ","),
		strings.Join(placeholders, ","),
	)

	connect := SafeConnect()

	stmt, err := connect.Prepare(query)
	CheckError(err)

	for _, value := range values {
		_, err = stmt.Exec(value...)
		CheckError(err)
	}
}

func assertTableRows(t *testing.T, tableName string, columns []string, values [][]interface{}, scan func(rows *sql.Rows) []interface{}) {
	connect := SafeConnect()
	defer connect.Close()

	query := fmt.Sprintf("SELECT %s FROM %s;", strings.Join(columns, ","), tableName)

	rows, err := connect.Query(query)
	CheckError(err)

	result := make([][]interface{}, 0, len(values))
	for rows.Next() {
		result = append(result, scan(rows))
	}

	if reflect.DeepEqual(result, values) == false {
		t.Error("Expected same rows:", values, "but got:", result)
	}
}

func assertSuccessStatus(t *testing.T, res *http.Response) {
	assertResponseStatusCode(t, http.StatusOK, res.StatusCode)
}

func assertResponseStatusCode(t *testing.T, expectStatusCode, actualStatusCode int) {
	if expectStatusCode != actualStatusCode {
		t.Error("Expect http status code:", expectStatusCode, "but got:", actualStatusCode)
	}
}

func assertResponse(t *testing.T, expect string, body io.ReadCloser) {
	responseString := response(body)

	if expect != responseString {
		t.Error("Expect responseString content:", expect, "but got:", responseString, "!")
	}
}

func response(body io.ReadCloser) string {
	value, err := ioutil.ReadAll(body)
	CheckError(err)
	return string(value)
}

func get(path string) *http.Response {
	res, err := http.Get(host(path))
	CheckError(err)
	return res
}

func post(path string, json string) *http.Response {
	reader := strings.NewReader(json)
	res, err := http.Post(host(path), "application/json", reader)
	CheckError(err)
	return res
}

func host(path string) string {
	return "http://localhost:8080" + path
}

func getCreateUserSongRequest() string {
	return `
		{
		  "songs": [
			{
			  "file_name": "yesterday.mp3",
			  "duration": 123,
			  "hz": 44100,
			  "bit_rate": 256,
			  "song_name": "Yesterday",
			  "artist_name": "The Beatles",
			  "song_year": 1965,
			  "genre": "Pop",
			  "link": "https://uk.wikipedia.org/wiki/Yesterday",
			  "composer": "John Lennon",
			  "copyright": "Jackson",
			  "publisher": "Capitol Records",
			  "album": {
				"name": "Help!",
				"artist_name": "The Beatles",
				"position": 13,
				"max_position": 14
			  }
			},
			{
			  "file_name": "desolation.mp3",
			  "duration": 183,
			  "hz": 44100,
			  "bit_rate": 256,
			  "song_name": null,
			  "artist_name": null,
			  "song_year": null,
			  "genre": null,
			  "link": null,
			  "composer": null,
			  "copyright": null,
			  "publisher": null,
			  "album": {
				"name": null,
				"artist_name": null,
				"position": null,
				"max_position": null
			  }
			}
		  ]
		}
	`
}
