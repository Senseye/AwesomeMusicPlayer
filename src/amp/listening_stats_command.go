package amp

import "time"

func ListeningStatisticUpdateCommand() error {
	connect := SafeConnect()
	defer connect.Close()

	_, err := connect.Exec(`TRUNCATE TABLE apm_stat_user_song_listening;`)

	if err != nil {
		return err
	}

	stmt, prepareError := connect.Prepare(`
		INSERT INTO apm_stat_user_song_listening (user_song_id, complete_count, skip_count, inserted, updated)
		SELECT user_song_id, SUM(complete_count), SUM(skip_count), $1, $1
		FROM apm_stat_user_song_listening_log
		GROUP BY user_song_id
		ORDER BY user_song_id;
`)

	// TODO: ON DUPLICATE KEY UPDATE

	if prepareError != nil {
		return prepareError
	}

	_, executeError := stmt.Exec(time.Now())

	return executeError
}
