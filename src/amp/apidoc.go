package amp

import (
	"net/http"
)

func ApiDoc(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./apidoc/index.html")
}
