package amp

import (
	"crypto/md5"
	"fmt"
	"path/filepath"
	"time"
)

func repositoryTracklistMainCreate(tracklist *TracklistMainRequest, userId int) error {
	connect := SafeConnect()
	defer connect.Close()

	now := time.Now()

	stmtTracklist := connect.QueryRow(
		`
			INSERT INTO apm_tracklist (named, name, inserted, updated)
			VALUES (false, NULL, $1, $1)
			RETURNING tracklist_id;
		`,
		now,
	)

	var mainTracklistId int
	stmtTracklist.Scan(&mainTracklistId)

	stmtUserToTracklist, err := connect.Prepare(`
		INSERT INTO apm_user_to_tracklist (user_id, tracklist_id, tracklist_position)
		VALUES ($1, $2, $3);
`)

	if err != nil {
		return err
	}

	_, err = stmtUserToTracklist.Exec(userId, mainTracklistId, 0)
	if err != nil {
		return err
	}

	insertSongSQL := `
		INSERT INTO amp_user_song (user_id, song_id, inserted)
		VALUES ($1, NULL, $2)
		RETURNING user_song_id;
`

	stmtSongMeta, err := connect.Prepare(`
		INSERT INTO amp_user_song_meta (
			user_song_id,
			file_name,
			file_extension,
			song_duration,
			hz,
			bit_rate,
			song_name,
			artist_name,
			album_name,
			album_artist_name,
			album_position,
			album_max_position,
			song_year,
			genre,
			link,
			composer,
			copyright,
			publisher
		) VALUES (
			$1,
			$2,
			$3,
			$4,
			$5,
			$6,
			$7,
			$8,
			$9,
			$10,
			$11,
			$12,
			$13,
			$14,
			$15,
			$16,
			$17,
			$18
		);
`)
	if err != nil {
		return err
	}

	stmtTracklistToUserSong, err := connect.Prepare(`
		INSERT INTO apm_tracklist_to_user_song (tracklist_id, user_song_id, user_song_position)
		VALUES ($1, $2, $3);
`)
	if err != nil {
		return err
	}

	for position, song := range tracklist.Songs {
		raw := connect.QueryRow(insertSongSQL, userId, now)
		var userSongId int
		raw.Scan(&userSongId)

		ext := filepath.Ext(song.FileName)

		if ext != "" {
			ext = ext[1:]
		}

		_, err = stmtSongMeta.Exec(
			userSongId,
			song.FileName,
			ext,
			song.Duration,
			song.Hz,
			song.BitRate,
			song.SongName,
			song.ArtistName,
			song.Album.Name,
			song.Album.ArtistName,
			song.Album.Position,
			song.Album.MaxPosition,
			song.SongYear,
			song.Genre,
			song.Link,
			song.Composer,
			song.Copyright,
			song.Publisher,
		)
		if err != nil {
			return err
		}

		_, err = stmtTracklistToUserSong.Exec(mainTracklistId, userSongId, position+1)
		if err != nil {
			return err
		}

		song.UserSongId = int(userSongId)
	}

	return nil
}

func repositoryStatisticSonglisteningLogStore(request *StatisticSongListetingLogListRequest) {
	connect := SafeConnect()
	defer connect.Close()

	stmt, err := connect.Prepare(`
		INSERT INTO apm_stat_user_song_listening_log (user_song_id, complete_count, skip_count, inserted)
		VALUES ($1, $2, $3, $4);
`)

	CheckError(err)

	now := time.Now()

	for _, song := range request.Songs {
		_, err = stmt.Exec(song.UserSongId, song.CompleteCount, song.SkipCount, now)
		CheckError(err)
	}
}

func getUserIdByToken(token string) int {
	connect := SafeConnect()
	defer connect.Close()

	rows, _ := connect.Query(`
		SELECT user_id
		FROM apm_user
		WHERE token=$1
		LIMIT 1`,
		token,
	)
	rows.Next()
	var userId int
	rows.Scan(&userId)
	return userId
}

func getUserTokenByEmail(email string) (string, error) {
	connect := SafeConnect()
	defer connect.Close()

	var (
		token string
		err   error
	)

	rows, _ := connect.Query(`
		SELECT token
		FROM apm_user
		WHERE email = $1
		LIMIT 1`,
		email,
	)
	rows.Next()
	rows.Scan(&token)

	if token == "" {
		token = fmt.Sprintf("%x", md5.Sum([]byte(email)))

		stmt, _ := connect.Prepare(`
		INSERT INTO apm_user (name, email, token)
		VALUES ($1, $2, $3);
`)

		_, err = stmt.Exec(email, email, token)
	}

	return token, err
}

func getUserSongListeningStatistic(userId int) []*StatisticSongListetingResponse {
	connect := SafeConnect()
	defer connect.Close()

	rows, err := connect.Query(
		`
			SELECT Stat.user_song_id, Stat.complete_count
			FROM apm_stat_user_song_listening Stat
			INNER JOIN amp_user_song UserSong USING (user_song_id)
			WHERE UserSong.user_id = $1;
		`,
		userId,
	)
	CheckError(err)

	result := []*StatisticSongListetingResponse{}
	for rows.Next() {
		var userSongId int
		var count int
		err := rows.Scan(&userSongId, &count)
		result = append(result, &StatisticSongListetingResponse{
			UserSongId:    userSongId,
			CompleteCount: count,
		})
		CheckError(err)
	}

	return result
}
