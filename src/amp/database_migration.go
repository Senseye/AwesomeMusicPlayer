package amp

import (
	"errors"
	"fmt"
	"strings"
)

func DatabaseMigrate() error {
	connect, err := Connect()
	defer connect.Close()

	if err != nil {
		return errors.New("Connect error:" + err.Error())
	}

	rows, err := connect.Query(`
		SELECT tablename
		FROM pg_catalog.pg_tables
		WHERE schemaname = 'public';
`)

	if err != nil {
		return errors.New("Get public tables error:" + err.Error())
	}

	tables := make([]string, 0, 64)

	for rows.Next() {
		var table string

		rows.Scan(&table)

		tables = append(tables, table)
	}

	if len(tables) > 0 {
		_, err = connect.Exec(fmt.Sprintf("DROP TABLE %s", strings.Join(tables, ",")))

		if err != nil {
			return errors.New("Drop tables error:" + err.Error())
		}
	}
/**
SELECT amp_user_song_meta.file_name
FROM amp_user_song
INNER JOIN amp_user_song_meta ON (amp_user_song_meta.user_song_id = amp_user_song.user_song_id)
WHERE amp_user_song.user_id = 10 AND amp_user_song_meta.file_name LIKE 'moving';

SELECT COUNT(*)
FROM amp_user_song
INNER JOIN amp_user_song_meta ON (amp_user_song_meta.user_song_id = amp_user_song.user_song_id)
WHERE amp_user_song.user_id = 10;

 */
	migrations := []string{
		`
		CREATE TABLE amp_song (
			song_id SERIAL PRIMARY KEY,
			artist_id INT NOT NULL,
			title TEXT NULL
		);
`,
		`
		CREATE TABLE amp_artist (
			artist_id SERIAL PRIMARY KEY,
			name TEXT NULL
		);
`,
		`
		CREATE TABLE amp_album (
			album_id SERIAL PRIMARY KEY,
			song_count INT NOT NULL,
			title TEXT NULL
		);
`,
		`
		CREATE TABLE amp_song_to_album (
			song_id SERIAL PRIMARY KEY,
			album_id INT NOT NULL,
			album_position INT NOT NULL
		);
`,
		`
		CREATE TABLE apm_user (
			user_id SERIAL PRIMARY KEY,
			email VARCHAR(255) NOT NULL UNIQUE,
			name VARCHAR(255) NOT NULL UNIQUE,
			token CHAR(32) NOT NULL
		);
`,
		`
		CREATE TABLE apm_tracklist (
			tracklist_id SERIAL PRIMARY KEY,
			named BOOLEAN NOT NULL,
			name VARCHAR(255) NULL,
			inserted TIMESTAMP NOT NULL,
			updated TIMESTAMP NOT NULL
		);
`,
		`
		CREATE TABLE amp_user_song (
			user_song_id SERIAL PRIMARY KEY,
			user_id INT NOT NULL,
			song_id INT NULL,
			inserted TIMESTAMP NOT NULL
		);
`,
		`
		CREATE TABLE amp_user_song_meta (
			user_song_id SERIAL PRIMARY KEY,
			file_name TEXT NOT NULL,
			file_extension CHAR(4) NOT NULL,
			song_duration INT NOT NULL,
			hz INT NOT NULL,
			bit_rate INT NOT NULL,
			song_name TEXT NULL,
			artist_name TEXT NULL,
			album_name TEXT NULL,
			album_artist_name TEXT NULL,
			album_position INT NULL,
			album_max_position INT NULL,
			song_year INT NULL,
			genre TEXT NULL,
			link TEXT NULL,
			composer TEXT NULL,
			copyright TEXT NULL,
			publisher TEXT NULL
		);
`,
		`
		CREATE TABLE apm_user_to_tracklist (
			id SERIAL PRIMARY KEY,
			user_id INT NOT NULL,
			tracklist_id INT NOT NULL,
			tracklist_position INT NOT NULL,
			UNIQUE (user_id, tracklist_id)
		);
`,
		`
		CREATE TABLE apm_tracklist_to_user_song (
			id SERIAL PRIMARY KEY,
			tracklist_id INT NOT NULL,
			user_song_id INT NOT NULL,
			user_song_position INT NOT NULL,
			UNIQUE (tracklist_id, user_song_id)
		);
`,
		`
		CREATE TABLE apm_stat_user_song_listening_log (
			id SERIAL PRIMARY KEY,
			user_song_id INT NOT NULL,
			complete_count INT NOT NULL,
			skip_count INT NOT NULL,
			inserted TIMESTAMP NOT NULL
		);
`,
		`
		CREATE TABLE apm_stat_user_song_listening (
			user_song_id SERIAL PRIMARY KEY,
			complete_count INT NOT NULL,
			skip_count INT NOT NULL,
			inserted TIMESTAMP NOT NULL,
			updated TIMESTAMP NOT NULL
		);
`,
		`
		INSERT INTO apm_user (email, token, name)
		VALUES ('senseye@awesome.player', '2519df20e8d1758304e579658c9fc669', 'Yaroslav');
`,
	}

	_, err = connect.Exec(strings.Join(migrations, ""))

	if err != nil {
		return errors.New("Migrate error:" + err.Error())
	}

	return nil
}
