package amp

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

func JsonDecode(r io.Reader, v interface{}) {
	decoder := json.NewDecoder(r)
	err := decoder.Decode(v)
	CheckError(err)
}

func JsonEncode(v interface{}) string {
	body, err := json.Marshal(v)
	CheckError(err)
	return string(body)
}

type AuthPrototypeRequest struct {
	Email string `json:"email"`
}

type TracklistMainSongAlmumRequest struct {
	Name        string `json:"name"`
	Position    int    `json:"position"`
	MaxPosition int    `json:"max_position"`
	ArtistName  string `json:"artist_name"`
}

type TracklistMainSongRequest struct {
	UserSongId int
	FileName   string                         `json:"file_name"`
	Duration   int                            `json:"duration"`
	Hz         int                            `json:"hz"`
	BitRate    int                            `json:"bit_rate"`
	SongName   string                         `json:"song_name"`
	ArtistName string                         `json:"artist_name"`
	SongYear   int                            `json:"song_year"`
	Genre      string                         `json:"genre"`
	Link       string                         `json:"link"`
	Composer   string                         `json:"composer"`
	Copyright  string                         `json:"copyright"`
	Publisher  string                         `json:"publisher"`
	Album      TracklistMainSongAlmumRequest `json:"album"`
}

type TracklistMainRequest struct {
	Songs []*TracklistMainSongRequest `json:"songs"`
}

type TracklistMainSongResponse struct {
	UserSongId int `json:"user_song_id"`
}

type TracklistMainResponse struct {
	Songs []*TracklistMainSongResponse `json:"songs"`
}

type StatisticSongListetingLogRequest struct {
	UserSongId    int `json:"user_song_id"`
	CompleteCount int `json:"complete_count"`
	SkipCount     int `json:"skip_count"`
}

type StatisticSongListetingLogListRequest struct {
	Songs []*StatisticSongListetingLogRequest `json:"songs"`
}

type StatisticSongListetingResponse struct {
	UserSongId    int `json:"user_song_id"`
	CompleteCount int `json:"complete_count"`
}

type StatisticSongListetingListResponse struct {
	Songs []*StatisticSongListetingResponse `json:"songs"`
}

func newTracklistMainResponse(tracklist *TracklistMainRequest) *TracklistMainResponse {
	length := len(tracklist.Songs)
	songs := make([]*TracklistMainSongResponse, 0, length)

	for _, song := range tracklist.Songs {
		songs = append(songs, &TracklistMainSongResponse{
			song.UserSongId,
		})
	}

	return &TracklistMainResponse{
		songs,
	}
}

func GetUserIdByRequest(r *http.Request) (int, error) {
	token := GetAuthRequestToken(r)

	if len(token) != TOKEN_LENGTH {
		return 0, errors.New(`{"error":"Invalid token format"}`)
	}

	userId := getUserIdByToken(token)

	if userId == 0 {
		return 0, errors.New(`{"error":"Wrong token"}`)
	}

	return userId, nil
}

func GetAuthRequestToken(r *http.Request) string {
	return r.URL.Query().Get("token")
}
