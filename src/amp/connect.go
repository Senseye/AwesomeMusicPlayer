package amp

import (
	"database/sql"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"os"
)

var (
	databaseUrl string
)

func getDatabaseUrl() string {
	url := os.Getenv("DATABASE_URL")

	if url == "" {
		return "postgres://postgres:password@postgres/apimusic?sslmode=disable"
	}

	connection, _ := pq.ParseURL(url)

	return connection + " sslmode=disable"
}

func SafeConnect() *sql.DB {
	db, _ := Connect()

	return db
}

func Connect() (*sql.DB, error) {
	if databaseUrl == "" {
		databaseUrl = getDatabaseUrl()
	}

	return sql.Open("postgres", databaseUrl)
}
